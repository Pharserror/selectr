import alias from '@rollup/plugin-alias';
import { configDefaults } from 'vitest/config';
import { defineConfig } from 'vite';
import nodeResolve from '@rollup/plugin-node-resolve';
import react from '@vitejs/plugin-react';
import { resolve } from 'path';


const projectRootDir = resolve(__dirname);

export default defineConfig({
  build: {
    lib: {
      // Build library
      entry: resolve(__dirname, 'src/v3/Selectr/index.ts'),
      // -----
      // Run demo server
      // entry: resolve(__dirname, 'demo.tsx'),
      // -----
      name: 'selectr',
      fileName: 'selectr',
    },
    minify: false,
    rollupOptions: {
      external: [
        'react',
      ],
      output: {
        globals: {
          react: 'React',
        },
      },
      plugins: [
        alias(),
      ],
    },
  },
  plugins: [
    react(),
  ],
  resolve: {
    alias: {
      '@src': resolve(projectRootDir, 'src'),
    }
  },
  test: {
    environment: 'jsdom',
    exclude: [...configDefaults.exclude],
    globals: true,
  },
});
