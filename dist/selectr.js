import React, { useRef, useCallback } from "react";
var jsxRuntime = { exports: {} };
var reactJsxRuntime_production_min = {};
/**
 * @license React
 * react-jsx-runtime.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var hasRequiredReactJsxRuntime_production_min;
function requireReactJsxRuntime_production_min() {
  if (hasRequiredReactJsxRuntime_production_min)
    return reactJsxRuntime_production_min;
  hasRequiredReactJsxRuntime_production_min = 1;
  var f = React, k = Symbol.for("react.element"), l = Symbol.for("react.fragment"), m = Object.prototype.hasOwnProperty, n = f.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED.ReactCurrentOwner, p = { key: true, ref: true, __self: true, __source: true };
  function q(c, a, g) {
    var b, d = {}, e = null, h = null;
    void 0 !== g && (e = "" + g);
    void 0 !== a.key && (e = "" + a.key);
    void 0 !== a.ref && (h = a.ref);
    for (b in a)
      m.call(a, b) && !p.hasOwnProperty(b) && (d[b] = a[b]);
    if (c && c.defaultProps)
      for (b in a = c.defaultProps, a)
        void 0 === d[b] && (d[b] = a[b]);
    return { $$typeof: k, type: c, key: e, ref: h, props: d, _owner: n.current };
  }
  reactJsxRuntime_production_min.Fragment = l;
  reactJsxRuntime_production_min.jsx = q;
  reactJsxRuntime_production_min.jsxs = q;
  return reactJsxRuntime_production_min;
}
var reactJsxRuntime_development = {};
/**
 * @license React
 * react-jsx-runtime.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var hasRequiredReactJsxRuntime_development;
function requireReactJsxRuntime_development() {
  if (hasRequiredReactJsxRuntime_development)
    return reactJsxRuntime_development;
  hasRequiredReactJsxRuntime_development = 1;
  if (process.env.NODE_ENV !== "production") {
    (function() {
      var React$1 = React;
      var REACT_ELEMENT_TYPE = Symbol.for("react.element");
      var REACT_PORTAL_TYPE = Symbol.for("react.portal");
      var REACT_FRAGMENT_TYPE = Symbol.for("react.fragment");
      var REACT_STRICT_MODE_TYPE = Symbol.for("react.strict_mode");
      var REACT_PROFILER_TYPE = Symbol.for("react.profiler");
      var REACT_PROVIDER_TYPE = Symbol.for("react.provider");
      var REACT_CONTEXT_TYPE = Symbol.for("react.context");
      var REACT_FORWARD_REF_TYPE = Symbol.for("react.forward_ref");
      var REACT_SUSPENSE_TYPE = Symbol.for("react.suspense");
      var REACT_SUSPENSE_LIST_TYPE = Symbol.for("react.suspense_list");
      var REACT_MEMO_TYPE = Symbol.for("react.memo");
      var REACT_LAZY_TYPE = Symbol.for("react.lazy");
      var REACT_OFFSCREEN_TYPE = Symbol.for("react.offscreen");
      var MAYBE_ITERATOR_SYMBOL = Symbol.iterator;
      var FAUX_ITERATOR_SYMBOL = "@@iterator";
      function getIteratorFn(maybeIterable) {
        if (maybeIterable === null || typeof maybeIterable !== "object") {
          return null;
        }
        var maybeIterator = MAYBE_ITERATOR_SYMBOL && maybeIterable[MAYBE_ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL];
        if (typeof maybeIterator === "function") {
          return maybeIterator;
        }
        return null;
      }
      var ReactSharedInternals = React$1.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED;
      function error(format) {
        {
          {
            for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
              args[_key2 - 1] = arguments[_key2];
            }
            printWarning("error", format, args);
          }
        }
      }
      function printWarning(level, format, args) {
        {
          var ReactDebugCurrentFrame2 = ReactSharedInternals.ReactDebugCurrentFrame;
          var stack = ReactDebugCurrentFrame2.getStackAddendum();
          if (stack !== "") {
            format += "%s";
            args = args.concat([stack]);
          }
          var argsWithFormat = args.map(function(item) {
            return String(item);
          });
          argsWithFormat.unshift("Warning: " + format);
          Function.prototype.apply.call(console[level], console, argsWithFormat);
        }
      }
      var enableScopeAPI = false;
      var enableCacheElement = false;
      var enableTransitionTracing = false;
      var enableLegacyHidden = false;
      var enableDebugTracing = false;
      var REACT_MODULE_REFERENCE;
      {
        REACT_MODULE_REFERENCE = Symbol.for("react.module.reference");
      }
      function isValidElementType(type) {
        if (typeof type === "string" || typeof type === "function") {
          return true;
        }
        if (type === REACT_FRAGMENT_TYPE || type === REACT_PROFILER_TYPE || enableDebugTracing || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || enableLegacyHidden || type === REACT_OFFSCREEN_TYPE || enableScopeAPI || enableCacheElement || enableTransitionTracing) {
          return true;
        }
        if (typeof type === "object" && type !== null) {
          if (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || // This needs to include all possible module reference object
          // types supported by any Flight configuration anywhere since
          // we don't know which Flight build this will end up being used
          // with.
          type.$$typeof === REACT_MODULE_REFERENCE || type.getModuleId !== void 0) {
            return true;
          }
        }
        return false;
      }
      function getWrappedName(outerType, innerType, wrapperName) {
        var displayName = outerType.displayName;
        if (displayName) {
          return displayName;
        }
        var functionName = innerType.displayName || innerType.name || "";
        return functionName !== "" ? wrapperName + "(" + functionName + ")" : wrapperName;
      }
      function getContextName(type) {
        return type.displayName || "Context";
      }
      function getComponentNameFromType(type) {
        if (type == null) {
          return null;
        }
        {
          if (typeof type.tag === "number") {
            error("Received an unexpected object in getComponentNameFromType(). This is likely a bug in React. Please file an issue.");
          }
        }
        if (typeof type === "function") {
          return type.displayName || type.name || null;
        }
        if (typeof type === "string") {
          return type;
        }
        switch (type) {
          case REACT_FRAGMENT_TYPE:
            return "Fragment";
          case REACT_PORTAL_TYPE:
            return "Portal";
          case REACT_PROFILER_TYPE:
            return "Profiler";
          case REACT_STRICT_MODE_TYPE:
            return "StrictMode";
          case REACT_SUSPENSE_TYPE:
            return "Suspense";
          case REACT_SUSPENSE_LIST_TYPE:
            return "SuspenseList";
        }
        if (typeof type === "object") {
          switch (type.$$typeof) {
            case REACT_CONTEXT_TYPE:
              var context = type;
              return getContextName(context) + ".Consumer";
            case REACT_PROVIDER_TYPE:
              var provider = type;
              return getContextName(provider._context) + ".Provider";
            case REACT_FORWARD_REF_TYPE:
              return getWrappedName(type, type.render, "ForwardRef");
            case REACT_MEMO_TYPE:
              var outerName = type.displayName || null;
              if (outerName !== null) {
                return outerName;
              }
              return getComponentNameFromType(type.type) || "Memo";
            case REACT_LAZY_TYPE: {
              var lazyComponent = type;
              var payload = lazyComponent._payload;
              var init = lazyComponent._init;
              try {
                return getComponentNameFromType(init(payload));
              } catch (x) {
                return null;
              }
            }
          }
        }
        return null;
      }
      var assign = Object.assign;
      var disabledDepth = 0;
      var prevLog;
      var prevInfo;
      var prevWarn;
      var prevError;
      var prevGroup;
      var prevGroupCollapsed;
      var prevGroupEnd;
      function disabledLog() {
      }
      disabledLog.__reactDisabledLog = true;
      function disableLogs() {
        {
          if (disabledDepth === 0) {
            prevLog = console.log;
            prevInfo = console.info;
            prevWarn = console.warn;
            prevError = console.error;
            prevGroup = console.group;
            prevGroupCollapsed = console.groupCollapsed;
            prevGroupEnd = console.groupEnd;
            var props = {
              configurable: true,
              enumerable: true,
              value: disabledLog,
              writable: true
            };
            Object.defineProperties(console, {
              info: props,
              log: props,
              warn: props,
              error: props,
              group: props,
              groupCollapsed: props,
              groupEnd: props
            });
          }
          disabledDepth++;
        }
      }
      function reenableLogs() {
        {
          disabledDepth--;
          if (disabledDepth === 0) {
            var props = {
              configurable: true,
              enumerable: true,
              writable: true
            };
            Object.defineProperties(console, {
              log: assign({}, props, {
                value: prevLog
              }),
              info: assign({}, props, {
                value: prevInfo
              }),
              warn: assign({}, props, {
                value: prevWarn
              }),
              error: assign({}, props, {
                value: prevError
              }),
              group: assign({}, props, {
                value: prevGroup
              }),
              groupCollapsed: assign({}, props, {
                value: prevGroupCollapsed
              }),
              groupEnd: assign({}, props, {
                value: prevGroupEnd
              })
            });
          }
          if (disabledDepth < 0) {
            error("disabledDepth fell below zero. This is a bug in React. Please file an issue.");
          }
        }
      }
      var ReactCurrentDispatcher = ReactSharedInternals.ReactCurrentDispatcher;
      var prefix;
      function describeBuiltInComponentFrame(name, source, ownerFn) {
        {
          if (prefix === void 0) {
            try {
              throw Error();
            } catch (x) {
              var match = x.stack.trim().match(/\n( *(at )?)/);
              prefix = match && match[1] || "";
            }
          }
          return "\n" + prefix + name;
        }
      }
      var reentry = false;
      var componentFrameCache;
      {
        var PossiblyWeakMap = typeof WeakMap === "function" ? WeakMap : Map;
        componentFrameCache = new PossiblyWeakMap();
      }
      function describeNativeComponentFrame(fn, construct) {
        if (!fn || reentry) {
          return "";
        }
        {
          var frame = componentFrameCache.get(fn);
          if (frame !== void 0) {
            return frame;
          }
        }
        var control;
        reentry = true;
        var previousPrepareStackTrace = Error.prepareStackTrace;
        Error.prepareStackTrace = void 0;
        var previousDispatcher;
        {
          previousDispatcher = ReactCurrentDispatcher.current;
          ReactCurrentDispatcher.current = null;
          disableLogs();
        }
        try {
          if (construct) {
            var Fake = function() {
              throw Error();
            };
            Object.defineProperty(Fake.prototype, "props", {
              set: function() {
                throw Error();
              }
            });
            if (typeof Reflect === "object" && Reflect.construct) {
              try {
                Reflect.construct(Fake, []);
              } catch (x) {
                control = x;
              }
              Reflect.construct(fn, [], Fake);
            } else {
              try {
                Fake.call();
              } catch (x) {
                control = x;
              }
              fn.call(Fake.prototype);
            }
          } else {
            try {
              throw Error();
            } catch (x) {
              control = x;
            }
            fn();
          }
        } catch (sample) {
          if (sample && control && typeof sample.stack === "string") {
            var sampleLines = sample.stack.split("\n");
            var controlLines = control.stack.split("\n");
            var s = sampleLines.length - 1;
            var c = controlLines.length - 1;
            while (s >= 1 && c >= 0 && sampleLines[s] !== controlLines[c]) {
              c--;
            }
            for (; s >= 1 && c >= 0; s--, c--) {
              if (sampleLines[s] !== controlLines[c]) {
                if (s !== 1 || c !== 1) {
                  do {
                    s--;
                    c--;
                    if (c < 0 || sampleLines[s] !== controlLines[c]) {
                      var _frame = "\n" + sampleLines[s].replace(" at new ", " at ");
                      if (fn.displayName && _frame.includes("<anonymous>")) {
                        _frame = _frame.replace("<anonymous>", fn.displayName);
                      }
                      {
                        if (typeof fn === "function") {
                          componentFrameCache.set(fn, _frame);
                        }
                      }
                      return _frame;
                    }
                  } while (s >= 1 && c >= 0);
                }
                break;
              }
            }
          }
        } finally {
          reentry = false;
          {
            ReactCurrentDispatcher.current = previousDispatcher;
            reenableLogs();
          }
          Error.prepareStackTrace = previousPrepareStackTrace;
        }
        var name = fn ? fn.displayName || fn.name : "";
        var syntheticFrame = name ? describeBuiltInComponentFrame(name) : "";
        {
          if (typeof fn === "function") {
            componentFrameCache.set(fn, syntheticFrame);
          }
        }
        return syntheticFrame;
      }
      function describeFunctionComponentFrame(fn, source, ownerFn) {
        {
          return describeNativeComponentFrame(fn, false);
        }
      }
      function shouldConstruct(Component) {
        var prototype = Component.prototype;
        return !!(prototype && prototype.isReactComponent);
      }
      function describeUnknownElementTypeFrameInDEV(type, source, ownerFn) {
        if (type == null) {
          return "";
        }
        if (typeof type === "function") {
          {
            return describeNativeComponentFrame(type, shouldConstruct(type));
          }
        }
        if (typeof type === "string") {
          return describeBuiltInComponentFrame(type);
        }
        switch (type) {
          case REACT_SUSPENSE_TYPE:
            return describeBuiltInComponentFrame("Suspense");
          case REACT_SUSPENSE_LIST_TYPE:
            return describeBuiltInComponentFrame("SuspenseList");
        }
        if (typeof type === "object") {
          switch (type.$$typeof) {
            case REACT_FORWARD_REF_TYPE:
              return describeFunctionComponentFrame(type.render);
            case REACT_MEMO_TYPE:
              return describeUnknownElementTypeFrameInDEV(type.type, source, ownerFn);
            case REACT_LAZY_TYPE: {
              var lazyComponent = type;
              var payload = lazyComponent._payload;
              var init = lazyComponent._init;
              try {
                return describeUnknownElementTypeFrameInDEV(init(payload), source, ownerFn);
              } catch (x) {
              }
            }
          }
        }
        return "";
      }
      var hasOwnProperty = Object.prototype.hasOwnProperty;
      var loggedTypeFailures = {};
      var ReactDebugCurrentFrame = ReactSharedInternals.ReactDebugCurrentFrame;
      function setCurrentlyValidatingElement(element) {
        {
          if (element) {
            var owner = element._owner;
            var stack = describeUnknownElementTypeFrameInDEV(element.type, element._source, owner ? owner.type : null);
            ReactDebugCurrentFrame.setExtraStackFrame(stack);
          } else {
            ReactDebugCurrentFrame.setExtraStackFrame(null);
          }
        }
      }
      function checkPropTypes(typeSpecs, values, location, componentName, element) {
        {
          var has = Function.call.bind(hasOwnProperty);
          for (var typeSpecName in typeSpecs) {
            if (has(typeSpecs, typeSpecName)) {
              var error$1 = void 0;
              try {
                if (typeof typeSpecs[typeSpecName] !== "function") {
                  var err = Error((componentName || "React class") + ": " + location + " type `" + typeSpecName + "` is invalid; it must be a function, usually from the `prop-types` package, but received `" + typeof typeSpecs[typeSpecName] + "`.This often happens because of typos such as `PropTypes.function` instead of `PropTypes.func`.");
                  err.name = "Invariant Violation";
                  throw err;
                }
                error$1 = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, "SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED");
              } catch (ex) {
                error$1 = ex;
              }
              if (error$1 && !(error$1 instanceof Error)) {
                setCurrentlyValidatingElement(element);
                error("%s: type specification of %s `%s` is invalid; the type checker function must return `null` or an `Error` but returned a %s. You may have forgotten to pass an argument to the type checker creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and shape all require an argument).", componentName || "React class", location, typeSpecName, typeof error$1);
                setCurrentlyValidatingElement(null);
              }
              if (error$1 instanceof Error && !(error$1.message in loggedTypeFailures)) {
                loggedTypeFailures[error$1.message] = true;
                setCurrentlyValidatingElement(element);
                error("Failed %s type: %s", location, error$1.message);
                setCurrentlyValidatingElement(null);
              }
            }
          }
        }
      }
      var isArrayImpl = Array.isArray;
      function isArray(a) {
        return isArrayImpl(a);
      }
      function typeName(value) {
        {
          var hasToStringTag = typeof Symbol === "function" && Symbol.toStringTag;
          var type = hasToStringTag && value[Symbol.toStringTag] || value.constructor.name || "Object";
          return type;
        }
      }
      function willCoercionThrow(value) {
        {
          try {
            testStringCoercion(value);
            return false;
          } catch (e) {
            return true;
          }
        }
      }
      function testStringCoercion(value) {
        return "" + value;
      }
      function checkKeyStringCoercion(value) {
        {
          if (willCoercionThrow(value)) {
            error("The provided key is an unsupported type %s. This value must be coerced to a string before before using it here.", typeName(value));
            return testStringCoercion(value);
          }
        }
      }
      var ReactCurrentOwner = ReactSharedInternals.ReactCurrentOwner;
      var RESERVED_PROPS = {
        key: true,
        ref: true,
        __self: true,
        __source: true
      };
      var specialPropKeyWarningShown;
      var specialPropRefWarningShown;
      var didWarnAboutStringRefs;
      {
        didWarnAboutStringRefs = {};
      }
      function hasValidRef(config) {
        {
          if (hasOwnProperty.call(config, "ref")) {
            var getter = Object.getOwnPropertyDescriptor(config, "ref").get;
            if (getter && getter.isReactWarning) {
              return false;
            }
          }
        }
        return config.ref !== void 0;
      }
      function hasValidKey(config) {
        {
          if (hasOwnProperty.call(config, "key")) {
            var getter = Object.getOwnPropertyDescriptor(config, "key").get;
            if (getter && getter.isReactWarning) {
              return false;
            }
          }
        }
        return config.key !== void 0;
      }
      function warnIfStringRefCannotBeAutoConverted(config, self) {
        {
          if (typeof config.ref === "string" && ReactCurrentOwner.current && self && ReactCurrentOwner.current.stateNode !== self) {
            var componentName = getComponentNameFromType(ReactCurrentOwner.current.type);
            if (!didWarnAboutStringRefs[componentName]) {
              error('Component "%s" contains the string ref "%s". Support for string refs will be removed in a future major release. This case cannot be automatically converted to an arrow function. We ask you to manually fix this case by using useRef() or createRef() instead. Learn more about using refs safely here: https://reactjs.org/link/strict-mode-string-ref', getComponentNameFromType(ReactCurrentOwner.current.type), config.ref);
              didWarnAboutStringRefs[componentName] = true;
            }
          }
        }
      }
      function defineKeyPropWarningGetter(props, displayName) {
        {
          var warnAboutAccessingKey = function() {
            if (!specialPropKeyWarningShown) {
              specialPropKeyWarningShown = true;
              error("%s: `key` is not a prop. Trying to access it will result in `undefined` being returned. If you need to access the same value within the child component, you should pass it as a different prop. (https://reactjs.org/link/special-props)", displayName);
            }
          };
          warnAboutAccessingKey.isReactWarning = true;
          Object.defineProperty(props, "key", {
            get: warnAboutAccessingKey,
            configurable: true
          });
        }
      }
      function defineRefPropWarningGetter(props, displayName) {
        {
          var warnAboutAccessingRef = function() {
            if (!specialPropRefWarningShown) {
              specialPropRefWarningShown = true;
              error("%s: `ref` is not a prop. Trying to access it will result in `undefined` being returned. If you need to access the same value within the child component, you should pass it as a different prop. (https://reactjs.org/link/special-props)", displayName);
            }
          };
          warnAboutAccessingRef.isReactWarning = true;
          Object.defineProperty(props, "ref", {
            get: warnAboutAccessingRef,
            configurable: true
          });
        }
      }
      var ReactElement = function(type, key, ref, self, source, owner, props) {
        var element = {
          // This tag allows us to uniquely identify this as a React Element
          $$typeof: REACT_ELEMENT_TYPE,
          // Built-in properties that belong on the element
          type,
          key,
          ref,
          props,
          // Record the component responsible for creating this element.
          _owner: owner
        };
        {
          element._store = {};
          Object.defineProperty(element._store, "validated", {
            configurable: false,
            enumerable: false,
            writable: true,
            value: false
          });
          Object.defineProperty(element, "_self", {
            configurable: false,
            enumerable: false,
            writable: false,
            value: self
          });
          Object.defineProperty(element, "_source", {
            configurable: false,
            enumerable: false,
            writable: false,
            value: source
          });
          if (Object.freeze) {
            Object.freeze(element.props);
            Object.freeze(element);
          }
        }
        return element;
      };
      function jsxDEV(type, config, maybeKey, source, self) {
        {
          var propName;
          var props = {};
          var key = null;
          var ref = null;
          if (maybeKey !== void 0) {
            {
              checkKeyStringCoercion(maybeKey);
            }
            key = "" + maybeKey;
          }
          if (hasValidKey(config)) {
            {
              checkKeyStringCoercion(config.key);
            }
            key = "" + config.key;
          }
          if (hasValidRef(config)) {
            ref = config.ref;
            warnIfStringRefCannotBeAutoConverted(config, self);
          }
          for (propName in config) {
            if (hasOwnProperty.call(config, propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
              props[propName] = config[propName];
            }
          }
          if (type && type.defaultProps) {
            var defaultProps = type.defaultProps;
            for (propName in defaultProps) {
              if (props[propName] === void 0) {
                props[propName] = defaultProps[propName];
              }
            }
          }
          if (key || ref) {
            var displayName = typeof type === "function" ? type.displayName || type.name || "Unknown" : type;
            if (key) {
              defineKeyPropWarningGetter(props, displayName);
            }
            if (ref) {
              defineRefPropWarningGetter(props, displayName);
            }
          }
          return ReactElement(type, key, ref, self, source, ReactCurrentOwner.current, props);
        }
      }
      var ReactCurrentOwner$1 = ReactSharedInternals.ReactCurrentOwner;
      var ReactDebugCurrentFrame$1 = ReactSharedInternals.ReactDebugCurrentFrame;
      function setCurrentlyValidatingElement$1(element) {
        {
          if (element) {
            var owner = element._owner;
            var stack = describeUnknownElementTypeFrameInDEV(element.type, element._source, owner ? owner.type : null);
            ReactDebugCurrentFrame$1.setExtraStackFrame(stack);
          } else {
            ReactDebugCurrentFrame$1.setExtraStackFrame(null);
          }
        }
      }
      var propTypesMisspellWarningShown;
      {
        propTypesMisspellWarningShown = false;
      }
      function isValidElement(object) {
        {
          return typeof object === "object" && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
        }
      }
      function getDeclarationErrorAddendum() {
        {
          if (ReactCurrentOwner$1.current) {
            var name = getComponentNameFromType(ReactCurrentOwner$1.current.type);
            if (name) {
              return "\n\nCheck the render method of `" + name + "`.";
            }
          }
          return "";
        }
      }
      function getSourceInfoErrorAddendum(source) {
        {
          if (source !== void 0) {
            var fileName = source.fileName.replace(/^.*[\\\/]/, "");
            var lineNumber = source.lineNumber;
            return "\n\nCheck your code at " + fileName + ":" + lineNumber + ".";
          }
          return "";
        }
      }
      var ownerHasKeyUseWarning = {};
      function getCurrentComponentErrorInfo(parentType) {
        {
          var info = getDeclarationErrorAddendum();
          if (!info) {
            var parentName = typeof parentType === "string" ? parentType : parentType.displayName || parentType.name;
            if (parentName) {
              info = "\n\nCheck the top-level render call using <" + parentName + ">.";
            }
          }
          return info;
        }
      }
      function validateExplicitKey(element, parentType) {
        {
          if (!element._store || element._store.validated || element.key != null) {
            return;
          }
          element._store.validated = true;
          var currentComponentErrorInfo = getCurrentComponentErrorInfo(parentType);
          if (ownerHasKeyUseWarning[currentComponentErrorInfo]) {
            return;
          }
          ownerHasKeyUseWarning[currentComponentErrorInfo] = true;
          var childOwner = "";
          if (element && element._owner && element._owner !== ReactCurrentOwner$1.current) {
            childOwner = " It was passed a child from " + getComponentNameFromType(element._owner.type) + ".";
          }
          setCurrentlyValidatingElement$1(element);
          error('Each child in a list should have a unique "key" prop.%s%s See https://reactjs.org/link/warning-keys for more information.', currentComponentErrorInfo, childOwner);
          setCurrentlyValidatingElement$1(null);
        }
      }
      function validateChildKeys(node, parentType) {
        {
          if (typeof node !== "object") {
            return;
          }
          if (isArray(node)) {
            for (var i = 0; i < node.length; i++) {
              var child = node[i];
              if (isValidElement(child)) {
                validateExplicitKey(child, parentType);
              }
            }
          } else if (isValidElement(node)) {
            if (node._store) {
              node._store.validated = true;
            }
          } else if (node) {
            var iteratorFn = getIteratorFn(node);
            if (typeof iteratorFn === "function") {
              if (iteratorFn !== node.entries) {
                var iterator = iteratorFn.call(node);
                var step;
                while (!(step = iterator.next()).done) {
                  if (isValidElement(step.value)) {
                    validateExplicitKey(step.value, parentType);
                  }
                }
              }
            }
          }
        }
      }
      function validatePropTypes(element) {
        {
          var type = element.type;
          if (type === null || type === void 0 || typeof type === "string") {
            return;
          }
          var propTypes;
          if (typeof type === "function") {
            propTypes = type.propTypes;
          } else if (typeof type === "object" && (type.$$typeof === REACT_FORWARD_REF_TYPE || // Note: Memo only checks outer props here.
          // Inner props are checked in the reconciler.
          type.$$typeof === REACT_MEMO_TYPE)) {
            propTypes = type.propTypes;
          } else {
            return;
          }
          if (propTypes) {
            var name = getComponentNameFromType(type);
            checkPropTypes(propTypes, element.props, "prop", name, element);
          } else if (type.PropTypes !== void 0 && !propTypesMisspellWarningShown) {
            propTypesMisspellWarningShown = true;
            var _name = getComponentNameFromType(type);
            error("Component %s declared `PropTypes` instead of `propTypes`. Did you misspell the property assignment?", _name || "Unknown");
          }
          if (typeof type.getDefaultProps === "function" && !type.getDefaultProps.isReactClassApproved) {
            error("getDefaultProps is only used on classic React.createClass definitions. Use a static property named `defaultProps` instead.");
          }
        }
      }
      function validateFragmentProps(fragment) {
        {
          var keys = Object.keys(fragment.props);
          for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            if (key !== "children" && key !== "key") {
              setCurrentlyValidatingElement$1(fragment);
              error("Invalid prop `%s` supplied to `React.Fragment`. React.Fragment can only have `key` and `children` props.", key);
              setCurrentlyValidatingElement$1(null);
              break;
            }
          }
          if (fragment.ref !== null) {
            setCurrentlyValidatingElement$1(fragment);
            error("Invalid attribute `ref` supplied to `React.Fragment`.");
            setCurrentlyValidatingElement$1(null);
          }
        }
      }
      var didWarnAboutKeySpread = {};
      function jsxWithValidation(type, props, key, isStaticChildren, source, self) {
        {
          var validType = isValidElementType(type);
          if (!validType) {
            var info = "";
            if (type === void 0 || typeof type === "object" && type !== null && Object.keys(type).length === 0) {
              info += " You likely forgot to export your component from the file it's defined in, or you might have mixed up default and named imports.";
            }
            var sourceInfo = getSourceInfoErrorAddendum(source);
            if (sourceInfo) {
              info += sourceInfo;
            } else {
              info += getDeclarationErrorAddendum();
            }
            var typeString;
            if (type === null) {
              typeString = "null";
            } else if (isArray(type)) {
              typeString = "array";
            } else if (type !== void 0 && type.$$typeof === REACT_ELEMENT_TYPE) {
              typeString = "<" + (getComponentNameFromType(type.type) || "Unknown") + " />";
              info = " Did you accidentally export a JSX literal instead of a component?";
            } else {
              typeString = typeof type;
            }
            error("React.jsx: type is invalid -- expected a string (for built-in components) or a class/function (for composite components) but got: %s.%s", typeString, info);
          }
          var element = jsxDEV(type, props, key, source, self);
          if (element == null) {
            return element;
          }
          if (validType) {
            var children = props.children;
            if (children !== void 0) {
              if (isStaticChildren) {
                if (isArray(children)) {
                  for (var i = 0; i < children.length; i++) {
                    validateChildKeys(children[i], type);
                  }
                  if (Object.freeze) {
                    Object.freeze(children);
                  }
                } else {
                  error("React.jsx: Static children should always be an array. You are likely explicitly calling React.jsxs or React.jsxDEV. Use the Babel transform instead.");
                }
              } else {
                validateChildKeys(children, type);
              }
            }
          }
          {
            if (hasOwnProperty.call(props, "key")) {
              var componentName = getComponentNameFromType(type);
              var keys = Object.keys(props).filter(function(k) {
                return k !== "key";
              });
              var beforeExample = keys.length > 0 ? "{key: someKey, " + keys.join(": ..., ") + ": ...}" : "{key: someKey}";
              if (!didWarnAboutKeySpread[componentName + beforeExample]) {
                var afterExample = keys.length > 0 ? "{" + keys.join(": ..., ") + ": ...}" : "{}";
                error('A props object containing a "key" prop is being spread into JSX:\n  let props = %s;\n  <%s {...props} />\nReact keys must be passed directly to JSX without using spread:\n  let props = %s;\n  <%s key={someKey} {...props} />', beforeExample, componentName, afterExample, componentName);
                didWarnAboutKeySpread[componentName + beforeExample] = true;
              }
            }
          }
          if (type === REACT_FRAGMENT_TYPE) {
            validateFragmentProps(element);
          } else {
            validatePropTypes(element);
          }
          return element;
        }
      }
      function jsxWithValidationStatic(type, props, key) {
        {
          return jsxWithValidation(type, props, key, true);
        }
      }
      function jsxWithValidationDynamic(type, props, key) {
        {
          return jsxWithValidation(type, props, key, false);
        }
      }
      var jsx = jsxWithValidationDynamic;
      var jsxs = jsxWithValidationStatic;
      reactJsxRuntime_development.Fragment = REACT_FRAGMENT_TYPE;
      reactJsxRuntime_development.jsx = jsx;
      reactJsxRuntime_development.jsxs = jsxs;
    })();
  }
  return reactJsxRuntime_development;
}
if (process.env.NODE_ENV === "production") {
  jsxRuntime.exports = requireReactJsxRuntime_production_min();
} else {
  jsxRuntime.exports = requireReactJsxRuntime_development();
}
var jsxRuntimeExports = jsxRuntime.exports;
function appendFetchedOptions(newOptions, options) {
  const { props, setState, state } = options;
  let availableOptionsValues = [];
  let newState = {
    availableOptions: { ...state.availableOptions },
    currentlySelectedListOption: state.filteredOptions.length
  };
  for (let group in props.groups) {
    if (!newState.availableOptions[group]) {
      newState.availableOptions[group] = {
        label: props.groups[group].label ?? props.defaultGroupKey,
        nodes: []
      };
    }
    newState.availableOptions[group].nodes.map((option) => {
      return availableOptionsValues.push(option.value);
    });
  }
  newOptions.filter((option) => availableOptionsValues.indexOf(option.value) === -1).forEach((option) => {
    const optionGroup = option.group ?? props.defaultGroupKey;
    if (!!newState.availableOptions[optionGroup]) {
      newState.availableOptions[optionGroup].nodes.push(option);
    }
  });
  newState.canLoadMoreOptions = newOptions.length === props.pageSize;
  newState.isAJAXing = false;
  setState((currentState) => {
    const updatedState = { ...currentState, ...newState };
    return {
      ...updatedState,
      ...filterOptions(null, "", { ...options, state: updatedState })
    };
  });
}
function filterOptions(event, filter, options) {
  const { props, setState, state } = options;
  const filterExp = !!event ? new RegExp(state.currentUserInput) : new RegExp(filter);
  const selectedOptionsValues = state.selectedOptions.map((option) => option.value);
  const availableOptions = [];
  for (let group in props.groups) {
    state.availableOptions[group].nodes.forEach((option) => {
      availableOptions.push(option);
    });
  }
  const newState = {
    // availableOptions,
    currentlySelectedListOption: 0,
    filteredOptions: availableOptions.filter((option) => !!option.label.match(filterExp) && !option.isNew && selectedOptionsValues.indexOf(option.value) === -1),
    isAJAXing: false
  };
  return newState;
}
function getSelectedOptionIndex(currentSelectedOptionIndex, delta, length) {
  const newIndex = currentSelectedOptionIndex + delta;
  if (newIndex < -1) {
    return -1;
  }
  if (newIndex > length) {
    return length;
  }
  return newIndex;
}
function loadMoreOptions(options) {
  const { props, setState, state } = options;
  if (!state.isAJAXing) {
    const { page } = state;
    setState((currentState) => ({ ...currentState, page, isAJAXing: true }));
    props.async(
      (newOptions) => appendFetchedOptions(newOptions, options),
      page,
      state.currentUserInput
    );
  }
}
function moveCursor(direction, options) {
  options.setState((currentState) => ({
    ...currentState,
    selectedOptionIndex: getSelectedOptionIndex(
      currentState.selectedOptionIndex,
      direction === "right" ? 1 : -1,
      currentState.selectedOptions.length
    )
  }));
}
function onBackspace(event, options) {
  const { setState, state } = options;
  const currentUserInput = event.target.value;
  if (!currentUserInput || currentUserInput === "") {
    const { selectedOptionIndex, selectedOptions } = state;
    const selectedOption = selectedOptions[selectedOptionIndex];
    if (!!selectedOption) {
      const newState = {
        selectedOptionIndex: getSelectedOptionIndex(
          selectedOptionIndex,
          -1,
          selectedOptions.length
        )
      };
      if (!!selectedOption.isNew) {
        newState.currentUserInput = selectedOption.value;
        options.refs.selectrInputRef.current.value = newState.currentUserInput;
      }
      setState((currentState) => ({
        ...currentState,
        ...newState
      }));
      removeSelectedOption(selectedOption, options);
    }
  }
}
function onBlur(event, options) {
  const { props } = options;
  if (props.onBlur) {
    props.onBlur(event, options);
  }
}
function onChange(event, options) {
  const { props, setState } = options;
  const currentUserInput = event.target.value;
  setState((currentState) => {
    const updatedState = {
      ...currentState,
      currentUserInput,
      page: 1
    };
    return {
      ...updatedState,
      ...filterOptions(event, "", { ...options, state: updatedState })
    };
  });
  if (props.onChange) {
    props.onChange(event, currentUserInput);
  }
}
function onClick(event, options) {
  if (document.activeElement !== event.target) {
    event.preventDefault();
    event.stopPropagation();
    return;
  }
  options.setState((currentState) => ({
    ...currentState,
    isListHidden: false
  }));
}
function onEnterTab(event, options) {
  const { props, refs, setState, state } = options;
  event.preventDefault();
  if (state.isListHidden) {
    return;
  }
  if (state.canLoadMoreOptions && state.currentlySelectedListOption === state.filteredOptions.length) {
    loadMoreOptions(options);
    return;
  }
  if (!!state.filteredOptions[state.currentlySelectedListOption] && state.currentUserInput === "") {
    selectOption(state.filteredOptions[state.currentlySelectedListOption], options);
    return;
  }
  if (state.filteredOptions.length === 1 && state.filteredOptions[0].value === state.currentUserInput) {
    selectOption(state.filteredOptions[0], options);
    return;
  }
  const newlySelectListOption = state.currentlySelectedListOption + 1;
  const newOption = {
    isNew: true,
    label: state.currentUserInput,
    value: state.currentUserInput,
    group: props.defaultGroupKey
  };
  const newState = {
    availableOptions: { ...state.availableOptions },
    currentlySelectedListOption: newlySelectListOption > state.filteredOptions.length ? state.filteredOptions.length : newlySelectListOption,
    currentUserInput: "",
    selectedOptionIndex: state.selectedOptions.length,
    selectedOptions: props.multiple ? [...state.selectedOptions].concat(newOption) : [newOption]
  };
  newState.availableOptions[props.defaultGroupKey].nodes.push(newOption);
  refs.selectrInputRef.current.value = "";
  setState((currentState) => {
    const updatedState = { ...currentState, ...newState };
    return {
      ...updatedState,
      ...filterOptions(null, "", { ...options, state: updatedState })
    };
  });
}
function onKeyDown(event, options) {
  const { props } = options;
  switch (event.keyCode) {
    case 8: {
      onBackspace(event, options);
      break;
    }
    case 13:
    case 9: {
      onEnterTab(event, options);
      break;
    }
    case 27: {
      toggleOptionsList(options, true);
      break;
    }
    case 37: {
      moveCursor("left", options);
      break;
    }
    case 38: {
      selectFromList(event, "prev", options);
      break;
    }
    case 39: {
      moveCursor("right", options);
      break;
    }
    case 40: {
      if (options.state.isListHidden) {
        toggleOptionsList(options);
      } else {
        selectFromList(event, "next", options);
      }
      break;
    }
  }
  if (props.onKeyDown) {
    props.onKeyDown(event);
  }
}
function removeSelectedOption(option, options) {
  const { props, setState, state } = options;
  let selectedOptionIndex;
  let selectedOptionsValues;
  let newState = {
    canLoadMoreOptions: true,
    filteredOptions: [...state.filteredOptions],
    selectedOptions: [...state.selectedOptions]
  };
  selectedOptionsValues = newState.selectedOptions.map((option2) => option2.value);
  selectedOptionIndex = selectedOptionsValues.indexOf(option.value);
  newState.selectedOptions.splice(selectedOptionIndex, 1);
  if (!option.isNew) {
    newState.filteredOptions = newState.filteredOptions.concat(option);
    newState.filteredOptions = newState.filteredOptions.sort((a, b) => {
      if (a.label < b.label) {
        return -1;
      }
      if (a.label > b.label) {
        return 1;
      }
      return 0;
    });
  } else {
    newState.availableOptions = { ...state.availableOptions };
    let availableOptionIndex;
    let optionGroup = option.group ?? props.defaultGroupKey;
    let availableOptionsValues = newState.availableOptions[optionGroup].nodes.map((option2) => option2.value);
    availableOptionIndex = availableOptionsValues.indexOf(option.value);
    newState.availableOptions[optionGroup].nodes.splice(availableOptionIndex, 1);
  }
  setState((currentState) => ({ ...currentState, ...newState }));
}
function scrollActiveListItemIntoView(event, options) {
  var _a;
  const { refs } = options;
  if (!!((_a = refs.activeListItemRef) == null ? void 0 : _a.current)) {
    refs.activeListItemRef.current.scrollIntoView(event.key === "ArrowDown");
  }
}
function selectFromList(event, selection, options) {
  const { setState, state } = options;
  let selectedOption = state.currentlySelectedListOption;
  switch (selection) {
    case "next":
      setState((currentState) => ({
        ...currentState,
        currentlySelectedListOption: selectedOption === state.filteredOptions.length ? selectedOption : selectedOption + 1
      }));
      scrollActiveListItemIntoView(event, options);
      break;
    case "prev":
      setState((currentState) => ({
        ...currentState,
        currentlySelectedListOption: selectedOption === -1 ? selectedOption : selectedOption - 1
      }));
      scrollActiveListItemIntoView(event, options);
      break;
  }
}
function selectOption(option, options) {
  var _a;
  const { props, setState, state } = options;
  const newlySelectListOption = state.currentlySelectedListOption + 1;
  const selectedOptions = props.multiple ? [...state.selectedOptions].concat(option) : [option];
  const newState = {
    selectedOptions,
    currentUserInput: "",
    currentlySelectedListOption: newlySelectListOption > state.filteredOptions.length ? state.filteredOptions.length : newlySelectListOption,
    selectedOptionIndex: state.selectedOptions.length
  };
  options.setState((currentState) => {
    const updatedState = { ...currentState, ...newState };
    return {
      ...updatedState,
      ...filterOptions(
        null,
        "",
        // This is a little odd IMO because we are interacting with the state
        // how it will be in the future once a React lifecycle has completed but
        // currently isn't when we call filterOptions - just watch for side-effects here
        { ...options, state: updatedState }
      )
    };
  });
  if (!props.stayOpenOnSelect) {
    options.refs.selectrInputRef.current.focus();
  }
  (_a = props.onSelectOption) == null ? void 0 : _a.call(props, option);
}
function toggleOptionsList(options, optionsListHidden) {
  options.setState((currentState) => ({
    ...currentState,
    invisibleScreenClass: "active",
    isListHidden: optionsListHidden ?? !currentState.isListHidden
  }));
}
const initialState = {
  availableOptions: { default: { label: "", nodes: [] } },
  canLoadMoreOptions: false,
  currentUserInput: "",
  currentlySelectedListOption: 0,
  filteredOptions: [],
  invisibleScreenClass: "hidden",
  isAJAXing: false,
  isListHidden: true,
  isPendingOptionsUpdate: false,
  page: 1,
  selectedOptionIndex: -1,
  selectedOptions: []
};
function getStyle(isListHidden, rootParentId) {
  var _a;
  let documentRect;
  const invisibleScreenStyle = {};
  let rootParentRect;
  if (!isListHidden) {
    documentRect = document.documentElement.getBoundingClientRect();
    invisibleScreenStyle.height = documentRect.height + "px";
    invisibleScreenStyle.width = documentRect.width + "px";
    rootParentRect = (_a = document.getElementById(rootParentId)) == null ? void 0 : _a.getBoundingClientRect();
    if (!rootParentRect) {
      invisibleScreenStyle.left = 0;
      invisibleScreenStyle.top = 0;
    } else {
      invisibleScreenStyle.left = 0 - rootParentRect.left + "px";
      invisibleScreenStyle.top = 0 - rootParentRect.top + "px";
    }
  }
  return invisibleScreenStyle;
}
function hideOptionsList(setState) {
  setState((currentState) => ({ ...currentState, isListHidden: true }));
}
const SelectrContext = React.createContext({});
function InvisibleScreen() {
  var _a;
  const {
    setState,
    state,
    props: { rootParentId },
    state: { isListHidden }
  } = React.useContext(SelectrContext);
  const hideOptionsList$1 = React.useCallback(() => {
    hideOptionsList(setState);
  }, [setState]);
  const style = React.useMemo(
    () => getStyle(isListHidden, rootParentId),
    [isListHidden, rootParentId]
  );
  return /* @__PURE__ */ jsxRuntimeExports.jsx(
    "div",
    {
      className: `${((_a = state.invisibleScreenClass) == null ? void 0 : _a.concat(" ")) ?? ""}invisible-screen`,
      onClick: hideOptionsList$1,
      style
    }
  );
}
function ListOptions() {
  const {
    props,
    refs,
    setState,
    state,
    props: { groups, optionsListItemClass },
    refs: { activeListItemRef },
    state: { currentlySelectedListOption, filteredOptions }
  } = React.useContext(SelectrContext);
  const groupedNodes = React.useMemo(() => {
    const newGroupedNodes = {};
    for (let group in groups) {
      newGroupedNodes[group] = [];
    }
    filteredOptions.forEach((option, index, options2) => {
      const isActive = currentlySelectedListOption === index;
      const optionGroup = option.group ?? props.defaultGroupKey;
      if (!newGroupedNodes[optionGroup]) {
        throw new Error(
          "ListOptions: data mismatch! An option has a group not passed to groups!"
        );
      }
      console.log("IS ACTIVE?", currentlySelectedListOption, index, isActive);
      newGroupedNodes[optionGroup].push(
        /* @__PURE__ */ jsxRuntimeExports.jsx(
          "li",
          {
            className: `${optionsListItemClass}${isActive ? " active" : ""}`,
            onClick: () => selectOption(
              option,
              { props, setState, state, refs }
            ),
            ref: isActive ? activeListItemRef : null,
            children: option.label
          },
          `${option.label.toLowerCase().split(" ").join("-")}-${index}`
        )
      );
    });
    return newGroupedNodes;
  }, [
    activeListItemRef,
    currentlySelectedListOption,
    filteredOptions,
    groups,
    optionsListItemClass
  ]);
  const options = Object.keys(groups).reduce((nodes, group) => {
    const label = groups[group].label;
    nodes.push(
      /* @__PURE__ */ jsxRuntimeExports.jsx(
        "li",
        {
          className: "list-item-option-group",
          children: label
        },
        label.toLowerCase().split(" ").join("-")
      )
    );
    return nodes.concat(groupedNodes[group]);
  }, []);
  return /* @__PURE__ */ jsxRuntimeExports.jsx(jsxRuntimeExports.Fragment, { children: options });
}
function Spinner() {
  const { props } = React.useContext(SelectrContext);
  if (!!props.AJAXSpinnerComponentFactory) {
    return props.AJAXSpinnerComponentFactory(props.AJAXSpinnerComponentProps);
  } else {
    return /* @__PURE__ */ jsxRuntimeExports.jsx("img", { className: props.AJAXSpinnerClasses, src: props.spinnerImgPath });
  }
}
function LoadMoreOptionsOption() {
  var _a, _b;
  const { props, refs, setState, state } = React.useContext(SelectrContext);
  if ((!props.infiniteScrolling || props.smartScroll) && !!props.async && !state.isAJAXing && state.canLoadMoreOptions) {
    const isActive = state.currentlySelectedListOption === state.filteredOptions.length;
    return /* @__PURE__ */ jsxRuntimeExports.jsx(
      "li",
      {
        className: `${props.loadMoreOptionsOptionClass}${isActive ? " active" : ""}`,
        onClick: () => loadMoreOptions({ props, setState, state }),
        children: props.manualAJAXPrompt
      }
    );
  } else if (state.isAJAXing) {
    return /* @__PURE__ */ jsxRuntimeExports.jsx(
      "li",
      {
        className: `${((_a = props.AJAXSpinnerListItemClasses) == null ? void 0 : _a.concat(" ")) ?? ""}ajax-spinner-list-item`,
        ref: refs.ajaxSpinnerRef,
        children: /* @__PURE__ */ jsxRuntimeExports.jsx(Spinner, {})
      }
    );
  } else if (state.filteredOptions.length === 0) {
    return /* @__PURE__ */ jsxRuntimeExports.jsx(
      "li",
      {
        className: `${((_b = props.noMoreOptionsListItemClasses) == null ? void 0 : _b.concat(" ")) ?? ""}no-more-options-list-item`,
        children: props.noMoreOptionsNotice
      }
    );
  }
}
function OptionsList() {
  var _a;
  const {
    state,
    refs: { inputContainerRef, optionsListRef }
  } = React.useContext(SelectrContext);
  return /* @__PURE__ */ jsxRuntimeExports.jsxs(
    "ul",
    {
      className: state.isListHidden ? "hidden" : "active",
      style: { width: ((_a = inputContainerRef.current) == null ? void 0 : _a.clientWidth) ?? "0px" },
      ref: optionsListRef,
      children: [
        /* @__PURE__ */ jsxRuntimeExports.jsx(ListOptions, {}),
        /* @__PURE__ */ jsxRuntimeExports.jsx(LoadMoreOptionsOption, {})
      ]
    }
  );
}
function OptionsListContainer() {
  var _a;
  const { props } = React.useContext(SelectrContext);
  return /* @__PURE__ */ jsxRuntimeExports.jsx(
    "div",
    {
      className: `${((_a = props.selectOptionsListWrapperClass) == null ? void 0 : _a.concat(" ")) ?? ""}options-list-container`,
      children: /* @__PURE__ */ jsxRuntimeExports.jsx(OptionsList, {})
    }
  );
}
function SelectedOptionTags() {
  const { props, setState, state } = React.useContext(SelectrContext);
  const tags = state.selectedOptions.map((option, index, options) => {
    var _a;
    return /* @__PURE__ */ jsxRuntimeExports.jsxs("li", { className: state.selectedOptionIndex === index ? "selected" : "", children: [
      /* @__PURE__ */ jsxRuntimeExports.jsx(
        "a",
        {
          className: `${((_a = props.closeIconClass) == null ? void 0 : _a.concat(" ")) ?? ""}close-icon`,
          href: "javascript:void(0)",
          onClick: () => removeSelectedOption(
            option,
            { props, setState, state }
          ),
          children: props.closeIconFactory({}, "x")
        },
        option.label.toLowerCase().split(" ").join("-")
      ),
      option.label
    ] });
  });
  return /* @__PURE__ */ jsxRuntimeExports.jsx(jsxRuntimeExports.Fragment, { children: tags });
}
function SelectGroupOptions(props) {
  const { state } = React.useContext(SelectrContext);
  let availableOptionsGroup = state.availableOptions[props.group];
  if (!!availableOptionsGroup) {
    let selectedOptionsValues = [];
    if (!!state.selectedOptions[0]) {
      selectedOptionsValues = state.selectedOptions.map((option) => option.value);
    }
    const options = availableOptionsGroup.nodes.map((option, index, options2) => /* @__PURE__ */ jsxRuntimeExports.jsx(
      "option",
      {
        selected: selectedOptionsValues.indexOf(option.value) > -1,
        value: option.value,
        children: option.label
      },
      `${option.label.toLowerCase().split(" ").join("-")}-${index}`
    ));
    return /* @__PURE__ */ jsxRuntimeExports.jsx(jsxRuntimeExports.Fragment, { children: options });
  }
  return null;
}
function SelectGroups() {
  const {
    props: { defaultGroupKey, groups },
    state: { availableOptions }
  } = React.useContext(SelectrContext);
  const selectGroups = React.useMemo(() => {
    var _a;
    let newGroups;
    if (!!groups) {
      newGroups = { ...groups };
    } else {
      newGroups = {};
      newGroups[defaultGroupKey] = {
        label: "",
        nodes: [...(_a = availableOptions[defaultGroupKey]) == null ? void 0 : _a.nodes]
      };
    }
    return newGroups;
  }, [availableOptions, defaultGroupKey, groups]);
  const optionGroups = Object.keys(selectGroups).map((group) => {
    const label = selectGroups[group].label;
    return /* @__PURE__ */ jsxRuntimeExports.jsx("optgroup", { label, children: /* @__PURE__ */ jsxRuntimeExports.jsx(SelectGroupOptions, { group }) }, label.toLowerCase().split(" ").join("-"));
  });
  return /* @__PURE__ */ jsxRuntimeExports.jsx(jsxRuntimeExports.Fragment, { children: optionGroups });
}
function Selectr(props) {
  var _a, _b;
  const { initialValue } = props;
  const [state, setState] = React.useState({
    ...initialState,
    ...!!initialValue ? {
      selectedOptions: [...initialValue]
    } : {}
  });
  const activeListItemRef = useRef(null);
  const ajaxSpinnerRef = useRef(null);
  const componentWrapperRef = useRef(null);
  const inputContainerRef = useRef(null);
  const optionsListRef = useRef(null);
  const selectElementRef = useRef(null);
  const selectedOptionsListRef = useRef(null);
  const selectrInputRef = useRef(null);
  const refs = {
    activeListItemRef,
    ajaxSpinnerRef,
    componentWrapperRef,
    inputContainerRef,
    optionsListRef,
    selectElementRef,
    selectedOptionsListRef,
    selectrInputRef
  };
  const optionsWithoutRefs = { props, setState, state };
  const optionsWithRefs = { ...optionsWithoutRefs, refs };
  const onBlur$1 = (event) => {
    onBlur(event, optionsWithoutRefs);
  };
  const onChange$1 = (event) => {
    onChange(event, optionsWithoutRefs);
  };
  const onClick$1 = (event) => {
    onClick(event, { setState });
  };
  const onKeyDown$1 = (event) => {
    onKeyDown(event, optionsWithRefs);
  };
  const toggleOptionsList$1 = useCallback(() => {
    toggleOptionsList({ setState });
  }, [setState]);
  React.useEffect(() => {
    if (!!props.options && props.options.length > 0) {
      appendFetchedOptions(props.options, optionsWithoutRefs);
    }
    if (!!props.async && props.options.length === 0) {
      loadMoreOptions(optionsWithoutRefs);
    }
  }, []);
  const contextValue = {
    props,
    refs,
    setState,
    state
  };
  return /* @__PURE__ */ jsxRuntimeExports.jsx(SelectrContext.Provider, { value: contextValue, children: /* @__PURE__ */ jsxRuntimeExports.jsxs(
    "div",
    {
      className: `${((_a = props.wrapperClass) == null ? void 0 : _a.concat(" ")) ?? ""}selectr`,
      ref: componentWrapperRef,
      children: [
        /* @__PURE__ */ jsxRuntimeExports.jsx(
          "select",
          {
            className: props.selectElementClass,
            id: props.id,
            multiple: !!props.multiple,
            name: props.selectElementName,
            ref: selectElementRef,
            children: /* @__PURE__ */ jsxRuntimeExports.jsx(SelectGroups, {})
          }
        ),
        /* @__PURE__ */ jsxRuntimeExports.jsx(
          "div",
          {
            className: `${((_b = props.inputWrapperClass) == null ? void 0 : _b.concat(" ")) ?? ""}${state.isListHidden ? "" : "active "}input-container`,
            ref: inputContainerRef,
            children: /* @__PURE__ */ jsxRuntimeExports.jsxs("ul", { ref: selectedOptionsListRef, children: [
              /* @__PURE__ */ jsxRuntimeExports.jsx(SelectedOptionTags, {}),
              /* @__PURE__ */ jsxRuntimeExports.jsx("li", { children: /* @__PURE__ */ jsxRuntimeExports.jsx(
                "input",
                {
                  className: props.inputClasses,
                  name: props.inputName,
                  onBlur: onBlur$1,
                  onChange: onChange$1,
                  onClick: onClick$1,
                  onFocus: toggleOptionsList$1,
                  onKeyDown: onKeyDown$1,
                  placeholder: props.placeholder,
                  ref: selectrInputRef,
                  type: "text"
                }
              ) })
            ] })
          }
        ),
        /* @__PURE__ */ jsxRuntimeExports.jsx(OptionsListContainer, {}),
        /* @__PURE__ */ jsxRuntimeExports.jsx(InvisibleScreen, {})
      ]
    }
  ) });
}
Selectr.defaultProps = {
  AJAXSpinnerClasses: "ajax-spinner",
  AJAXSpinnerComponentFactory: void 0,
  AJAXSpinnerComponentProps: {},
  AJAXSpinnerListItemClasses: "",
  async: void 0,
  closeIconFactory: React.createFactory("em"),
  closeIconClass: "",
  debounceTimeout: 500,
  defaultGroupKey: "default",
  groups: { default: { label: "", nodes: [] } },
  infiniteScrolling: false,
  initialValue: [],
  inputWrapperClass: "",
  isSubmitAsync: true,
  loadMoreOptionsOptionClass: "",
  manualAJAXPrompt: "Load more options",
  multiple: false,
  noMoreOptionsNotice: "No more options available",
  noMoreOptionsListItemClasses: "",
  options: [],
  optionsListItemClass: "list-item",
  pageSize: 10,
  placeholder: "Please select from the dropdown or type to filter",
  rootParentId: "root",
  selectElementClass: "hidden",
  selectElementName: "selectr",
  selectOptionsListWrapperClass: "",
  shouldLogErrors: false,
  smartScroll: false,
  spinnerImgPath: "./images/loader.gif",
  stayOpenOnSelect: false,
  submitMethod: "POST",
  submitPassword: void 0,
  submitUrl: "http://localhost",
  submitUser: void 0,
  wrapperClass: ""
};
export {
  Selectr as default
};
