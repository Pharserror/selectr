import { createRoot } from 'react-dom/client';
import React from 'react';
import { v4 as uuid } from 'uuid';

import type { Option } from './src/v3/Selectr/types';
import SelectR from './src/v3/Selectr';
import * as selectrConcerns from './src/v3/Selectr/concerns';
import spinnerImgPath from './images/loader.gif';


function generateRandomOptions(callback?: Function) {
  const options: Option[] = [];
  const max = 999;
  const min = 0;
  const seed = Math.floor(Math.random() * (max - min + 1)) + min;

  for (let i = 0; i <= 9; i++) {
    options.push({
      group: 'default',
      id: uuid(),
      label: 'test-' + seed + '-' + i,
      value: 'test-' + seed + '-' + i,
    });
  }

  if (!!callback) {
    // Fake a wait time so it seems more realistic and you can see the spinner
    setTimeout(callback.bind(null, options), 4000);
  } else {
    return options;
  }
}

document.addEventListener('DOMContentLoaded', function () {
  const options = generateRandomOptions();
  const root = createRoot(document.getElementById('root'));

  root.render(
    <form
      onSubmit={event => {
        event.preventDefault();
        selectrConcerns.onSubmit(
          event,
          {
            props: {
              options,
              submitSelection: results => alert(results.map(result => result.value).join(' ')),
            },
          },
        );
      }}
    >
      <SelectR
        infiniteScrolling
        multiple
        smartScroll
        async={callback => {
          generateRandomOptions(
            fetchedOptions => {
              options.push(...fetchedOptions);
              callback(fetchedOptions);
            },
          );
        }}
        groups={{ default: { label: 'TEST', nodes: [] } }}
        initialValue={[options[0]]}
        options={options}
        rootParentId="root"
        spinnerImgPath={spinnerImgPath}
      />
      <input type="submit" value="submit" />
    </form>,
  );
});
