import React from 'react';

import { SelectrContext } from '../Selectr/context';


export default function Spinner() {
  const { props } = React.useContext(SelectrContext);
  // The user can pass in their own React factory for something like React-Loader
  // if they don't want to use the packaged static image
   if (!!props.AJAXSpinnerComponentFactory) {
    return props.AJAXSpinnerComponentFactory(props.AJAXSpinnerComponentProps);
  } else {
    return (
      <img className={props.AJAXSpinnerClasses} src={props.spinnerImgPath} />
    );
  }
}

