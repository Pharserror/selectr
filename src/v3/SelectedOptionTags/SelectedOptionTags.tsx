import React from 'react';

import * as selectrConcerns from '../Selectr/concerns';
import { SelectrContext } from '../Selectr/context';


export default function SelectedOptionTags() {
  const { props, setState, state } = React.useContext(SelectrContext);

  const tags = state.selectedOptions.map((option, index, options) => (
    <li className={state.selectedOptionIndex === index ? 'selected' : ''}>
      <a
        className={`${(props.closeIconClass?.concat(' ') ?? '')}close-icon`}
        href="javascript:void(0)"
        key={option.label.toLowerCase().split(' ').join('-')}
        onClick={() => selectrConcerns.removeSelectedOption(
          option, { props, setState, state },
        )}
      >
        {props.closeIconFactory({}, 'x')}
      </a>
      {option.label}
    </li>
  ));

  return <>{tags}</>;
}

