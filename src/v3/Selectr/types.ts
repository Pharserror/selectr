import React from 'react';


export type ComponentRefs = {
  activeListItemRef:      React.MutableRefObject<ComponentElementTypes['activeListItemRef']>;
  ajaxSpinnerRef:         React.MutableRefObject<ComponentElementTypes['ajaxSpinnerRef']>;
  componentWrapperRef:    React.MutableRefObject<ComponentElementTypes['componentWrapperRef']>;
  inputContainerRef:      React.MutableRefObject<ComponentElementTypes['inputContainerRef']>;
  optionsListRef:         React.MutableRefObject<ComponentElementTypes['optionsListRef']>;
  selectElementRef:       React.MutableRefObject<ComponentElementTypes['selectElementRef']>;
  selectedOptionsListRef: React.MutableRefObject<ComponentElementTypes['selectedOptionsListRef']>;
  selectrInputRef:        React.MutableRefObject<ComponentElementTypes['selectrInputRef']>;
};

export type ComponentElementTypes = {
  activeListItemRef:      HTMLLIElement;
  ajaxSpinnerRef:         HTMLLIElement;
  componentWrapperRef:    HTMLDivElement;
  inputContainerRef:      HTMLDivElement;
  optionsListRef:         HTMLUListElement;
  selectElementRef:       HTMLSelectElement;
  selectedOptionsListRef: HTMLUListElement;
  selectrInputRef:        HTMLInputElement;
};

export type Group = {
  [key: string]: {
    label: string;
    nodes: Option[];
  };
};

export type Option = {
  group?: string;
  id?:    string;
  isNew?: boolean;
  label:  string;
  value:  string;
};

export type OptionsWithoutRefs = {
  props:    Props;
  setState: React.Dispatch<React.SetStateAction<State>>;
  state:    State;
};

export type Options = OptionsWithoutRefs & {
  refs: ComponentRefs;
};

export interface Props {
  AJAXSpinnerClasses?:            string;
  AJAXSpinnerComponentFactory?:   Function;
  AJAXSpinnerComponentProps?:     object;
  AJAXSpinnerListItemClasses?:    string;
  async?:                         Function;
  closeIconFactory?:              Function;
  closeIconClass?:                string;
  debounceFunc?:                  Function;
  debounceTimeout?:               number;
  defaultGroupKey?:               string;
  groups?:                        Group;
  id?:                            string;
  handleSubmitResponse?:          Function;
  infiniteScrolling?:             boolean;
  initialValue?:                  Option[];
  inputClasses?:                  string;
  inputRef?:                      ComponentRefs['selectrInputRef'];
  inputName?:                     string;
  inputWrapperClass?:             string;
  isSubmitAsync?:                 boolean;
  loadMoreOptionsOptionClass?:    string;
  manualAJAXPrompt?:              string;
  multiple?:                      boolean;
  noMoreOptionsNotice?:           string;
  noMoreOptionsListItemClasses?:  string;
  onBlur?:                        Function;
  onChange?:                      Function;
  onClick?:                       Function;
  onKeyDown?:                     Function;
  onSelectOption?:                Function;
  options:                        Option[];
  optionsListItemClass?:          string;
  pageSize?:                      number;
  placeholder?:                   string;
  rootParentId?:                  string;
  selectElementClass?:            string;
  selectElementName?:             string;
  selectionFormatter?:            Function;
  selectOptionsListWrapperClass?: string;
  shouldLogErrors?:               boolean;
  smartScroll?:                   boolean;
  spinnerImgPath?:                string;
  stayOpenOnSelect?:              boolean;
  submitMethod?:                  string;
  submitPassword?:                string;
  submitSelection?:               Function;
  submitUrl?:                     string;
  submitUser?:                    string;
  wrapperClass?:                  string;
}

export type SelectrContextValue = {
  props: Props;
  refs: ComponentRefs;
  setState: React.Dispatch<React.SetStateAction<State>>;
  state: State;
};

export type State = {
  availableOptions:             Group;
  canLoadMoreOptions:           boolean;
  currentUserInput:             string;
  currentlySelectedListOption:  number;
  filteredOptions:              Option[];
  invisibleScreenClass:         string;
  isAJAXing:                    boolean;
  isListHidden:                 boolean;
  isPendingOptionsUpdate:       boolean;
  page:                         number;
  selectedOptionIndex:          number;
  selectedOptions:              Option[];
};
