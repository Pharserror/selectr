// import '../../selectr.scss';

import React, { Component, useCallback, useRef } from 'react';

import type {
  ComponentElementTypes,
  Options,
  OptionsWithoutRefs,
  Props,
  SelectrContextValue,
  State,
} from './types';
import * as concerns from './concerns';
import { initialState } from './state';
import InvisibleScreen from '../InvisibleScreen';
import OptionsListContainer from '../OptionsListContainer';
import SelectedOptionTags from '../SelectedOptionTags';
import SelectGroups from '../SelectGroups';
import { SelectrContext } from './context';


export default function Selectr(props: Props) {
  // PROPS
  const { initialValue } = props;

  // STATE
  const [state, setState] = React.useState<State>({
    ...initialState,
    ...(!!initialValue ? {
      selectedOptions: [...initialValue],
    } : {}),
  });

  // REFS
  const activeListItemRef      = useRef<ComponentElementTypes['activeListItemRef']>(null);
  const ajaxSpinnerRef         = useRef<ComponentElementTypes['ajaxSpinnerRef']>(null);
  const componentWrapperRef    = useRef<ComponentElementTypes['componentWrapperRef']>(null);
  const inputContainerRef      = useRef<ComponentElementTypes['inputContainerRef']>(null);
  const optionsListRef         = useRef<ComponentElementTypes['optionsListRef']>(null);
  const selectElementRef       = useRef<ComponentElementTypes['selectElementRef']>(null);
  const selectedOptionsListRef = useRef<ComponentElementTypes['selectedOptionsListRef']>(null);
  // TODO: Have this take a forward ref?
  const selectrInputRef        = useRef<ComponentElementTypes['selectrInputRef']>(null);
  const refs = {
    activeListItemRef,
    ajaxSpinnerRef,
    componentWrapperRef,
    inputContainerRef,
    optionsListRef,
    selectElementRef,
    selectedOptionsListRef,
    selectrInputRef,
  };

  const optionsWithoutRefs: OptionsWithoutRefs = { props, setState, state };
  const optionsWithRefs: Options = { ...optionsWithoutRefs, refs };
  const onBlur = event => {
    concerns.onBlur(event, optionsWithoutRefs);
  };

  const onChange = event => {
    concerns.onChange(event, optionsWithoutRefs);
  };

  const onClick = event => {
    concerns.onClick(event, { setState });
  };

  const onKeyDown = event => {
    concerns.onKeyDown(event, optionsWithRefs);
  };

  const toggleOptionsList = useCallback(() => {
    concerns.toggleOptionsList({ setState });
  }, [setState]);

  // EFFECTS
  React.useEffect(() => {
    // FIXME: see if this needs to happen on startup or should be elsewhere
    if (!!props.options && props.options.length > 0) {
      concerns.appendFetchedOptions(props.options, optionsWithoutRefs);
    }

    if (!!props.async && props.options.length === 0) {
      concerns.loadMoreOptions(optionsWithoutRefs);
    }
  }, []);

  //  componentDidUpdate() {
  //    if (props.shouldLoadMoreOptions) {
  //      this.loadMoreOptions();
  //    }
  //  }

  // CONTEXT
  // FIXME: Maybe use Immutable here
  const contextValue: SelectrContextValue = {
    props,
    refs,
    setState,
    state,
  };

  return (
    <SelectrContext.Provider value={contextValue}>
      <div
        className={`${(props.wrapperClass?.concat(' ') ?? '')}selectr`}
        ref={componentWrapperRef}
      >
        <select
          className={props.selectElementClass}
          id={props.id}
          multiple={!!props.multiple}
          name={props.selectElementName}
          ref={selectElementRef}
        >
          <SelectGroups />
        </select>
        <div
          className={(
            `${(props.inputWrapperClass?.concat(' ') ?? '')}${(state.isListHidden ? '' : 'active ')}input-container`
          )}
          ref={inputContainerRef}
        >
          <ul ref={selectedOptionsListRef}>
            <SelectedOptionTags />
            <li>
              <input
                className={props.inputClasses}
                name={props.inputName}
                onBlur={onBlur}
                onChange={onChange}
                onClick={onClick}
                onFocus={toggleOptionsList}
                onKeyDown={onKeyDown}
                placeholder={props.placeholder}
                ref={selectrInputRef}
                type="text"
              />
            </li>
          </ul>
        </div>
        <OptionsListContainer />
        <InvisibleScreen />
      </div>
    </SelectrContext.Provider>
  );
}

Selectr.defaultProps = {
  AJAXSpinnerClasses:            'ajax-spinner',
  AJAXSpinnerComponentFactory:   undefined,
  AJAXSpinnerComponentProps:     {},
  AJAXSpinnerListItemClasses:    '',
  async:                         undefined,
  closeIconFactory:              React.createFactory('em'),
  closeIconClass:                '',
  debounceTimeout:               500,
  defaultGroupKey:               'default',
  groups:                        { default: { label: '', nodes: [] } },
  infiniteScrolling:             false,
  initialValue:                  [],
  inputWrapperClass:             '',
  isSubmitAsync:                 true,
  loadMoreOptionsOptionClass:    '',
  manualAJAXPrompt:              'Load more options',
  multiple:                      false,
  noMoreOptionsNotice:           'No more options available',
  noMoreOptionsListItemClasses:  '',
  options:                       [],
  optionsListItemClass:          'list-item',
  pageSize:                      10,
  placeholder:                   'Please select from the dropdown or type to filter',
  rootParentId:                  'root',
  selectElementClass:            'hidden',
  selectElementName:             'selectr',
  selectOptionsListWrapperClass: '',
  shouldLogErrors:               false,
  smartScroll:                   false,
  spinnerImgPath:                './images/loader.gif',
  stayOpenOnSelect:              false,
  submitMethod:                  'POST',
  submitPassword:                undefined,
  submitUrl:                     'http://localhost',
  submitUser:                    undefined,
  wrapperClass:                  ''
};
