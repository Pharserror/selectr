import type {
  ChangeEvent as ReactSyntheticChangeEvent,
  FormEvent as ReactSyntheticFormEvent,
  KeyboardEvent as ReactSyntheticKeyboardEvent,
} from 'react';

import type { Option, Options, OptionsWithoutRefs, State } from './types';


// FIXME: Rename `options` to `params` everywhere
export function appendFetchedOptions(newOptions: Option[], options: OptionsWithoutRefs) {
  const { props, setState, state } = options;
  let availableOptionsValues = [];
  // We want to append any options to what we already have
  let newState: Partial<State> = {
    availableOptions: { ...state.availableOptions },
    currentlySelectedListOption: state.filteredOptions.length,
  };

  for (let group in props.groups) {
    // If the group doesn't exist we initialize it
    if (!newState.availableOptions[group]) {
      newState.availableOptions[group] = {
        label: props.groups[group].label ?? props.defaultGroupKey,
        nodes: []
      };
    }
    // Otherwise we add what we have to the list of available options
    newState.availableOptions[group].nodes.map(option => {
      return availableOptionsValues.push(option.value);
    });
  }
  // We discard whatever we've received that is already in the list of available
  // options so that we don't display the same thing twice
  newOptions.filter(option => (
    availableOptionsValues.indexOf(option.value) === -1
  )).forEach(option => {
    const optionGroup = option.group ?? props.defaultGroupKey;

    if (!!newState.availableOptions[optionGroup]) {
      newState.availableOptions[optionGroup].nodes.push(option);
    }
  });

  newState.canLoadMoreOptions = newOptions.length === props.pageSize;
  newState.isAJAXing = false;
  /* TODO: If this was working right then loadMoreOptions would not trigger
   * again from onScroll after calling scrollIntoView from loadMoreOptions
   */
  setState(currentState => {
    const updatedState = { ...currentState, ...newState };

    return {
      ...updatedState,
      ...filterOptions(null, '', { ...options, state: updatedState }),
    };
  });
}

export function debounceFunc(func: Function, time: number) {
  let timeout;

  return () => {
    clearTimeout(timeout);
    timeout = setTimeout(func, time);
  }
}

export function dispatcher(chain: Array<Function>) {
  let toExecute = chain.pop();

   if (!!toExecute) {
    toExecute();
    dispatcher(chain);
  }
}

export function filterOptions(
  event: ReactSyntheticChangeEvent | null,
  filter: string,
  options: OptionsWithoutRefs,
) {
  const { props, setState, state } = options;
  const filterExp = (
    !!event
    ? new RegExp(state.currentUserInput)
    : new RegExp(filter)
  );

  const selectedOptionsValues = state.selectedOptions.map(option => option.value);
  const availableOptions = [];

   for (let group in props.groups) {
    state.availableOptions[group].nodes.forEach(option => {
      availableOptions.push(option);
    });
  }

  const newState: Partial<State> = {
    // availableOptions,
    currentlySelectedListOption: 0,
    filteredOptions: availableOptions.filter(option => (
      !!option.label.match(filterExp)
      && !option.isNew
      && selectedOptionsValues.indexOf(option.value) === -1
    )),
    isAJAXing: false,
  };

  return newState;
}

export function getSelectedOptionIndex(
  currentSelectedOptionIndex: number,
  delta: number,
  length: number,
) {
  const newIndex = currentSelectedOptionIndex + delta;

  if (newIndex < -1) { return -1; }
  if (newIndex > length) { return length; }
  return newIndex;
}

export function handleSubmitResponse(options: Partial<Options>) {
  options.setState?.(currentState => ({ ...currentState, messages: 'success' }));
  options.props.handleSubmitResponse?.(options);
}

export function isBottomOfListVisible(options: Options) {
  let optionsList = options.refs.optionsListRef.current;
  // Should be equal to $options-list-max-height
  let optionsListHeight = optionsList.clientHeight;
  let isVisible = (
    (optionsListHeight > 0)
    && (
      (optionsList.scrollHeight - optionsList.clientHeight) === optionsList.scrollTop
    )
  );

  return isVisible;
}

export function loadMoreOptions(options: OptionsWithoutRefs) {
  const { props, setState, state } = options;

  if (!state.isAJAXing) {
    const { page } = state;

    setState(currentState => ({ ...currentState, page, isAJAXing: true }));
    props.async(
      newOptions => appendFetchedOptions(newOptions, options),
      page,
      state.currentUserInput,
    );
    // The spinner should be showing now so we want the user to see it
    // TODO: scrolling causes the onScroll to trigger; need to do
    // this.refs.AJAXSpinner.scrollIntoView();
  }
}

export function moveCursor(direction: 'left' | 'right', options: Options) {
  options.setState(currentState => ({
    ...currentState,
    selectedOptionIndex: getSelectedOptionIndex(
      currentState.selectedOptionIndex,
      (direction === 'right' ? 1 : -1),
      currentState.selectedOptions.length,
    ),
  }));
}

export function onBackspace(
  event: ReactSyntheticKeyboardEvent<HTMLInputElement>,
  options: Options
) {
  const { setState, state } = options;
  const currentUserInput = (event.target as HTMLInputElement).value;

  if (!currentUserInput || currentUserInput === '') {
    const { selectedOptionIndex, selectedOptions } = state;
    const selectedOption = selectedOptions[selectedOptionIndex];

    if (!!selectedOption) {
      const newState: Partial<State> = {
        selectedOptionIndex: getSelectedOptionIndex(
          selectedOptionIndex, -1, selectedOptions.length,
        ),
      };

      if (!!selectedOption.isNew) {
        newState.currentUserInput = selectedOption.value;
        options.refs.selectrInputRef.current.value = newState.currentUserInput;
      }

      setState(currentState => ({
        ...currentState,
        ...newState,
      }));

      removeSelectedOption(selectedOption, options);
    }
  }
}

export function onBlur(event: React.FocusEvent, options: OptionsWithoutRefs) {
  // TODO: store window.keyDown and bind this.keyDown
  const { props } = options;

  if (props.onBlur) {
    props.onBlur(event, options);
  }
}

export function onChange(
  event: ReactSyntheticChangeEvent<HTMLInputElement>,
  options: OptionsWithoutRefs,
) {
  const { props, setState } = options;
  const currentUserInput = event.target.value;

  setState(currentState => {
    const updatedState = {
      ...currentState,
      currentUserInput,
      page: 1,
    };

    return {
      ...updatedState,
      ...filterOptions(event, '', { ...options, state: updatedState }),
    };
  });

  if (props.onChange) {
    props.onChange(event, currentUserInput);
  }
}

export function onClick(event: React.MouseEvent, options: Pick<Options, 'setState'>) {
  if (document.activeElement !== event.target) {
    event.preventDefault();
    event.stopPropagation();
    return;
  }

  options.setState(currentState => ({
    ...currentState,
    isListHidden: false,
  }));
}

export function onEnterTab(
  event: ReactSyntheticKeyboardEvent<HTMLInputElement>,
  options: Options,
) {
  const { props, refs, setState, state } = options;

  event.preventDefault();

  if (state.isListHidden) {
    return;
  }

  // The last option in the list, if there are more options to load, will be the
  // `<LoadMoreOptionsOption />` component and it has its own `onKeyDown` event
  // that will fire `loadMoreOptions` so we don't want to do anything here
  if (
    state.canLoadMoreOptions
    && state.currentlySelectedListOption === state.filteredOptions.length
  ) {
    loadMoreOptions(options);
    return;
  }

  if (
    !!state.filteredOptions[state.currentlySelectedListOption]
    && state.currentUserInput === ''
  ) {
    selectOption(state.filteredOptions[state.currentlySelectedListOption], options);
    return;
  }

  if (
    state.filteredOptions.length === 1
    && state.filteredOptions[0].value === state.currentUserInput
  ) {
    selectOption(state.filteredOptions[0], options);
    return;
  }

  // Once the user hits enter we need try to highlight the next option in the list
  const newlySelectListOption = state.currentlySelectedListOption + 1;
  const newOption: Option = {
    isNew: true,
    label: state.currentUserInput,
    value: state.currentUserInput,
    group: props.defaultGroupKey
  };

  const newState: Partial<State> = {
    availableOptions: { ...state.availableOptions },
    currentlySelectedListOption: (
      newlySelectListOption > state.filteredOptions.length
        ? state.filteredOptions.length
        : newlySelectListOption
    ),
    currentUserInput: '',
    selectedOptionIndex: state.selectedOptions.length,
    selectedOptions: (
      props.multiple
      ? [...state.selectedOptions].concat(newOption)
      : [newOption]
    )
  };

  // TODO: potentially need to remove this option from the availableOptions
  // list if a user deletes it
  newState.availableOptions[props.defaultGroupKey].nodes.push(newOption);

  refs.selectrInputRef.current.value = '';

  setState(currentState => {
    const updatedState = { ...currentState, ...newState };

    return {
      ...updatedState,
      ...filterOptions(null, '', { ...options, state: updatedState }),
    };
  });
}

// FIXME: Make it so that when a user is pressing the up or down arrow the option
// that is selected will scroll into view
export function onKeyDown(
  event: ReactSyntheticKeyboardEvent<HTMLInputElement>,
  options: Options,
) {
  const { props } = options;

  switch (event.keyCode) {
    case 8: { // backspace
      onBackspace(event, options);
      break;
    }
    case 13: // enter
    case 9: { // tab
      onEnterTab(event, options);
      break;
    }
    case 27: { // escape
      toggleOptionsList(options, true);
      break;
    }
    case 37: { // arrow left
      moveCursor('left', options);
      break;
    }
    case 38: { // arrow up
      selectFromList(event, 'prev', options);
      break;
    }
    case 39: { // arrow right
      moveCursor('right', options);
      break;
    }
    case 40: { // arrow down
      if (options.state.isListHidden) {
        toggleOptionsList(options);
      } else {
        selectFromList(event, 'next', options);
      }

      break;
    }
  }

  if (props.onKeyDown) {
    props.onKeyDown(event);
  }
}

export function onScroll(options: Options) {
   if (options.props.infiniteScrolling && isBottomOfListVisible(options)) {
    loadMoreOptions(options);
  }
}

export function onSubmit(
  event: ReactSyntheticFormEvent<HTMLFormElement>,
  options: Partial<Options>,
) {
  let results = selectionFormatter(event, options);

  submitSelection(results, options);
}

export function removeSelectedOption(option: Option, options: OptionsWithoutRefs) {
  const { props, setState, state } = options;
  let selectedOptionIndex;
  let selectedOptionsValues;
  let removedOptionIndex;
  let newState: Partial<State> = {
    canLoadMoreOptions: true,
    filteredOptions: [...state.filteredOptions],
    selectedOptions: [...state.selectedOptions],
  };

  selectedOptionsValues = newState.selectedOptions.map(option => option.value);
  selectedOptionIndex = selectedOptionsValues.indexOf(option.value);
  newState.selectedOptions.splice(selectedOptionIndex, 1);

   if (!option.isNew) {
    newState.filteredOptions = newState.filteredOptions.concat(option);
    // If this is a pre-existing option we want it to go back into the right place
    newState.filteredOptions = newState.filteredOptions.sort((a, b) => {
      if (a.label < b.label) { return -1; }
      if (a.label > b.label) { return 1; }
      return 0;
    });
  } else {
    newState.availableOptions = { ...state.availableOptions };
    let availableOptionIndex;
    let optionGroup = option.group ?? props.defaultGroupKey;
    let availableOptionsValues = (
      newState
      .availableOptions[optionGroup]
      .nodes
      .map(option => option.value)
    );

    availableOptionIndex = availableOptionsValues.indexOf(option.value);
    // New options get deleted
    newState.availableOptions[optionGroup].nodes.splice(availableOptionIndex, 1);
  }

  setState(currentState => ({ ...currentState, ...newState }));
}

export function scrollActiveListItemIntoView(
  event: ReactSyntheticKeyboardEvent<HTMLInputElement>,
  options: Options,
) {
  const { refs } = options;

  if (!!refs.activeListItemRef?.current) {
    refs.activeListItemRef.current.scrollIntoView(event.key === 'ArrowDown');
  }
}

export function selectFromList(
  event: ReactSyntheticKeyboardEvent<HTMLInputElement>,
  selection: 'next' | 'prev',
  options: Options,
) {
  const { setState, state } = options;
  let selectedOption = state.currentlySelectedListOption;

  switch (selection) {
    case 'next':
      setState(currentState => ({
        ...currentState,
        currentlySelectedListOption: (
          selectedOption === (state.filteredOptions.length)
          ? selectedOption
          : (selectedOption + 1)
        ),
      }));

      scrollActiveListItemIntoView(event, options);

      break;
    case 'prev':
      setState(currentState => ({
        ...currentState,
        currentlySelectedListOption: (
          selectedOption === -1
          ? selectedOption
          : (selectedOption - 1)
        ),
      }));

      scrollActiveListItemIntoView(event, options);

      break;
  }
}

export function selectionFormatter(
  event: ReactSyntheticFormEvent<HTMLFormElement>,
  options: Partial<Options>,
) {
  let selectedOptions: Option[] = [];

  try {
    const formData = new FormData(event.target as HTMLFormElement);

    for (const entry of formData.entries()) {
      options.props.options.forEach(option => {
        // FIXME: This needs to account for an option's group as well
        if (option.value === entry[1]) {
          selectedOptions.push(option);
        }
      });
    }
  } catch (error) {
    if (!!options.props.shouldLogErrors) {
      console.log(error);
    }
  }

  return selectedOptions;
}

export function selectOption(option: Option, options: Options) {
  const { props, setState, state } = options;
  const newlySelectListOption = state.currentlySelectedListOption + 1;
  const selectedOptions = (
    props.multiple
    ? [...state.selectedOptions].concat(option)
    : [option]
  );

  const newState: Partial<State> = {
    selectedOptions,
    currentUserInput: '',
    currentlySelectedListOption: (
      newlySelectListOption > state.filteredOptions.length
        ? state.filteredOptions.length
        : newlySelectListOption
    ),
    selectedOptionIndex: state.selectedOptions.length,
  };

  options.setState(currentState => {
    const updatedState = { ...currentState, ...newState };

    return {
      ...updatedState,
      ...filterOptions(
        null,
        '',
        // This is a little odd IMO because we are interacting with the state
        // how it will be in the future once a React lifecycle has completed but
        // currently isn't when we call filterOptions - just watch for side-effects here
        { ...options, state: updatedState },
      ),
    };
  });

  if (!props.stayOpenOnSelect) {
    options.refs.selectrInputRef.current.focus();
  }

  props.onSelectOption?.(option);
}

export function submitSelection(selection: Option[], options: Partial<Options>) {
  const { props } = options;

  if (props.submitSelection) {
    props.submitSelection(selection);
    return;
  }

  const request = new XMLHttpRequest();

  request.addEventListener('load', () => handleSubmitResponse(options));

  if (
    props.submitMethod
      && props.submitUrl
      && props.isSubmitAsync
      && props.submitUser
      && props.submitPassword
  ) {
    request.open(
      props.submitMethod,
      props.submitUrl,
      props.isSubmitAsync,
      props.submitUser,
      props.submitPassword
    );

    return request.send(selection.map(option => option.value).join(','));
  }

  throw new Error('Missing props in submitSelection!');
}

export function toggleOptionsList(
  options: Pick<Options, 'setState'>,
  optionsListHidden?: boolean,
) {
  options.setState(currentState => ({
    ...currentState,
    invisibleScreenClass: 'active',
    isListHidden: optionsListHidden ?? !currentState.isListHidden,
  }));
}
