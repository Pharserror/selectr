import { State } from './types';


export const initialState: State = {
  availableOptions:             { default: { label: '', nodes: [] } },
  canLoadMoreOptions:           false,
  currentUserInput:             '',
  currentlySelectedListOption:  0,
  filteredOptions:              [],
  invisibleScreenClass:         'hidden',
  isAJAXing:                    false,
  isListHidden:                 true,
  isPendingOptionsUpdate:       false,
  page:                         1,
  selectedOptionIndex:          -1,
  selectedOptions:              [],
};
