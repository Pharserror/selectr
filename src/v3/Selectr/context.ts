import React from 'react';

import { SelectrContextValue } from './types';


export const SelectrContext = React.createContext({} as SelectrContextValue);
