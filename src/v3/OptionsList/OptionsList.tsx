import React from 'react';

import ListOptions from '../ListOptions';
import LoadMoreOptionsOption from '../LoadMoreOptionsOption';
import * as selectrConcerns from '../Selectr/concerns';
import { SelectrContext } from '../Selectr/context';


export default function OptionsList() {
  const {
    state,
    refs: { inputContainerRef, optionsListRef },
  } = React.useContext(SelectrContext);

  return (
    <ul
      className={state.isListHidden ? 'hidden' : 'active'}
      style={{ width: inputContainerRef.current?.clientWidth ?? '0px' }}
      ref={optionsListRef}
    >
      <ListOptions />
      <LoadMoreOptionsOption />
    </ul>
  );
}

