import React, { ReactElement } from 'react';

import * as selectrConcerns from '../Selectr/concerns';
import { SelectrContext } from '../Selectr/context';


export default function ListOptions() {
  const {
    props,
    refs,
    setState,
    state,
    props: { groups, optionsListItemClass },
    refs: { activeListItemRef },
    state: { currentlySelectedListOption, filteredOptions },
  } = React.useContext(SelectrContext);

  const groupedNodes = React.useMemo(() => {
    const newGroupedNodes = {};

    for (let group in groups) {
      newGroupedNodes[group] = [];
    }

    filteredOptions.forEach((option, index, options) => {
      const isActive = currentlySelectedListOption === index;
      const optionGroup = option.group ?? props.defaultGroupKey;

      if (!newGroupedNodes[optionGroup]) {
        throw new Error(
          'ListOptions: data mismatch! An option has a group not passed to groups!'
        );
      }

      console.log('IS ACTIVE?', currentlySelectedListOption, index, isActive);
      newGroupedNodes[optionGroup].push(
        <li
          className={`${optionsListItemClass}${(isActive ? ' active': '')}`}
          key={`${option.label.toLowerCase().split(' ').join('-')}-${index}`}
          onClick={() => selectrConcerns.selectOption(
            option,
            { props, setState, state, refs },
          )}
          ref={(isActive ? activeListItemRef : null)}
        >
          {option.label}
        </li>
      );
    });

    return newGroupedNodes;
  }, [
    activeListItemRef,
    currentlySelectedListOption,
    filteredOptions,
    groups,
    optionsListItemClass,
  ]);


  const options = Object.keys(groups).reduce<ReactElement[]>((nodes, group) => {
    const label = groups[group].label;

    nodes.push(
      <li
        className="list-item-option-group"
        key={label.toLowerCase().split(' ').join('-')}
      >
        {label}
      </li>
    );

    return nodes.concat(groupedNodes[group]);
  }, []);

  return <>{options}</>;
}
