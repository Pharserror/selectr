import React from 'react';

import * as concerns from './concerns';
import { SelectrContext } from '../Selectr/context';


export default function InvisibleScreen() {
  const {
    setState,
    state,
    props: { rootParentId },
    state: { isListHidden },
  } = React.useContext(SelectrContext);

  const hideOptionsList = React.useCallback(() => {
    concerns.hideOptionsList(setState);
  }, [setState]);

  const style = React.useMemo(
    () => concerns.getStyle(isListHidden, rootParentId),
    [isListHidden, rootParentId],
  );

  return (
    <div
      className={(
        `${(state.invisibleScreenClass?.concat(' ') ?? '')}invisible-screen`
      )}
      onClick={hideOptionsList}
      style={style}
    ></div>
  );
}
