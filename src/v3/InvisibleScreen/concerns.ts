import React from 'react';

export function getStyle(isListHidden: boolean, rootParentId: string) {
  let documentRect;
  const invisibleScreenStyle: Pick<
    React.CSSProperties,
    | 'height'
    | 'left'
    | 'top'
    | 'width'
  > = {};

  let rootParentRect;

  if (!isListHidden) {
    documentRect = document.documentElement.getBoundingClientRect();
    invisibleScreenStyle.height = documentRect.height + 'px';
    invisibleScreenStyle.width = documentRect.width + 'px';
    rootParentRect = document.getElementById(rootParentId)?.getBoundingClientRect();

    if (!rootParentRect) {
      invisibleScreenStyle.left = 0;
      invisibleScreenStyle.top = 0;
    } else {
      invisibleScreenStyle.left = (0 - rootParentRect.left) + 'px';
      invisibleScreenStyle.top = (0 - rootParentRect.top) + 'px';
    }
  }

  return invisibleScreenStyle;
}

export function hideOptionsList(setState) {
  setState(currentState => ({ ...currentState, isListHidden: true }));
}

