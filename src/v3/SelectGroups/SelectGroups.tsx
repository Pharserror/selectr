import React from 'react';

import SelectGroupOptions from '../SelectGroupOptions';
import { SelectrContext } from '../Selectr/context';


export default function SelectGroups() {
  const {
    props: { defaultGroupKey, groups },
    state: { availableOptions },
  } = React.useContext(SelectrContext);

  const selectGroups = React.useMemo(() => {
    let newGroups;

    if (!!groups) {
      newGroups = { ...groups };
    } else {
      newGroups = {};
      newGroups[defaultGroupKey] = {
        label: '',
        nodes: [...availableOptions[defaultGroupKey]?.nodes],
      };
    }

    return newGroups;
  }, [availableOptions, defaultGroupKey, groups]);

  const optionGroups = Object.keys(selectGroups).map(group => {
    const label = selectGroups[group].label;

    return (
      <optgroup key={label.toLowerCase().split(' ').join('-')} label={label}>
        <SelectGroupOptions group={group} />
      </optgroup>
    );
  });

  return <>{optionGroups}</>;
}
