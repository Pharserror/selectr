import React from 'react';

import OptionsList from '../OptionsList';
import { SelectrContext } from '../Selectr/context';


export default function OptionsListContainer() {
  const { props } = React.useContext(SelectrContext);

  return (
    <div
      className={(
        `${(props.selectOptionsListWrapperClass?.concat(' ') ?? '')}options-list-container`
      )}
    >
      <OptionsList />
    </div>
  );
}
