import React from 'react';

import { Props } from './types';
import { SelectrContext } from '../Selectr/context';


export default function SelectGroupOptions(props: Props) {
  const { state } = React.useContext(SelectrContext);
  let availableOptionsGroup = state.availableOptions[props.group];

  if (!!availableOptionsGroup) {
    let selectedOptionsValues = [];

    if (!!state.selectedOptions[0]) {
      selectedOptionsValues = state.selectedOptions.map(option => option.value);
    }

    const options = availableOptionsGroup.nodes.map((option, index, options) => (
      <option
        key={`${option.label.toLowerCase().split(' ').join('-')}-${index}`}
        selected={selectedOptionsValues.indexOf(option.value) > -1}
        value={option.value}
      >
        {option.label}
      </option>
    ));

    return <>{options}</>;
  }

  return null;
}
