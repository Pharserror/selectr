import React from 'react';

import * as selectrConcerns from '../Selectr/concerns';
import Spinner from '../Spinner';
import { SelectrContext } from '../Selectr/context';


export default function LoadMoreOptionsOption() {
  const { props, refs, setState, state } = React.useContext(SelectrContext);

  if (
    (!props.infiniteScrolling || props.smartScroll)
    && !!props.async
    && !state.isAJAXing
    && state.canLoadMoreOptions
  ) {
    const isActive = state.currentlySelectedListOption === state.filteredOptions.length;

    return (
      <li
        className={`${props.loadMoreOptionsOptionClass}${(isActive ? ' active': '')}`}
        onClick={() => selectrConcerns.loadMoreOptions({ props, setState, state })}
      >
        {props.manualAJAXPrompt}
      </li>
    );
  } else if (state.isAJAXing) {
    return (
      <li
        className={(
          `${(props.AJAXSpinnerListItemClasses?.concat(' ') ?? '')}ajax-spinner-list-item`
        )}
        ref={refs.ajaxSpinnerRef}
      >
        <Spinner />
      </li>
    );
  } else if (state.filteredOptions.length === 0) {
    // TODO: (hybrid) If user has removed an option and AJAX'd again then display the
    // notice, but not all the time
    return (
      <li
        className={(
          `${(props.noMoreOptionsListItemClasses?.concat(' ') ?? '')}no-more-options-list-item`
        )}
      >
        {props.noMoreOptionsNotice}
      </li>
    );
  }
}
